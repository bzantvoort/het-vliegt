﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DroneMonitorTool {
    public partial class LogForm : Form {
        public LogForm() {
            InitializeComponent();
        }

        public void SetLogValue (string m){
           //rtbLog.Text += String.Format("{0} \n",m);
            if (this.Visible)
            rtbLog.AppendText(m);
        }

        private void rtbLog_TextChanged(object sender, EventArgs e) {

        }  
    }
}
