﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Diagnostics;

namespace DroneMonitorTool {
    public partial class Form1 : Form {
        LogForm logForm = new LogForm();

        public Form1() {
            InitializeComponent();
        }
        private void ScanComPorts() {
            String[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);

            cbPorts.Items.Clear();
            foreach (String port in ports) {
                cbPorts.Items.Add(port);
            }

            if (cbBaudrate.Items.Count > 0) {
                cbPorts.Text = "Select!";
                cbPorts.Enabled = true;
            }
            else {
                cbPorts.Text = "N.A.";
                cbPorts.Enabled = false;
            }
        }

        private void btnScanPorts_Click(object sender, EventArgs e) {
            ScanComPorts();
            cbPorts.Enabled = true;
            cbBaudrate.Enabled = true;
            btnConnect.Enabled = true;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e) {
            serialPort.Close();
        }

        private void btnConnect_Click(object sender, EventArgs e) {
            serialPort.Close();
            serialPort.PortName = cbPorts.SelectedItem.ToString();

            serialPort.BaudRate = Convert.ToInt32(cbBaudrate.SelectedItem);

            if (!serialPort.IsOpen) {
                try {
                    serialPort.Open();
                    lbConnected.Text = String.Format("{0} - F450 Flamewheel", serialPort.PortName);
                }
                catch {
                    MessageBox.Show("There was an error. Please make sure that the correct port was selected, and the device, plugged in.");
                }
            }
            timer.Enabled = true;
        }

        private void timer_Tick(object sender, EventArgs e) {
            SearchYourTags();
        }

        private void SearchYourTags() {
            string m_data;
            string[] m_values;

            m_data = serialPort.ReadLine();

            if (m_data.IndexOf('#') == 0 && m_data.IndexOf('%') > 0) {

                m_values = m_data.Split('|');
                FillForms(m_values);

                logForm.SetLogValue(m_data);
                serialPort.DiscardInBuffer();
            }
        }

        private void btnLog_Click(object sender, EventArgs e) {
            logForm.ShowDialog();

            logForm.SetLogValue("F450 Flamewheel Connection Log");
            logForm.SetLogValue("------------------------------");
        }

        private void FillForms(string[] data) {

            data[0] = data[0].Replace("#","");
            data[13] = data[13].Replace("%\r\r", "");

            lbServoStab1.Text = data[0];
            lbServoStab2.Text = data[1];
            lbServoStab3.Text = data[2];
            lbServoStab4.Text = data[3];

            lbServo24Pos.Text = data[4];
            tbServo24Pos.Value = Convert.ToInt32(data[4]);
            lbServo13Pos.Text = data[5];
            tbServo13Pos.Value = Convert.ToInt32(data[5]);

            lbHoogtePos.Text = data[6];
            tbHoogteAccel.Value = Convert.ToInt32(data[6]);

            lbServo24Accel.Text = data[7];
            tbServo24Accel.Value = Convert.ToInt32(data[7]);
            lbServo13Accel.Text = data[8];
            tbServo13Accel.Value = Convert.ToInt32(data[8]);

            lbRotation.Text = data[9];
            tbRotation.Value = Convert.ToInt32(data[9]);

            lbServo1.Text = data[10];
            lbTrbServo1.Text = (Convert.ToInt32(data[10]) + Convert.ToInt32(data[0])).ToString();
            tbServo1.Value = Convert.ToInt32(data[10]) + Convert.ToInt32(data[0]);
            lbServo2.Text = data[11];
            lbTrbServo2.Text = (Convert.ToInt32(data[11]) + Convert.ToInt32(data[1])).ToString();
            tbServo2.Value = Convert.ToInt32(data[11]) + Convert.ToInt32(data[1]);
            lbServo3.Text = data[12];
            lbTrbServo3.Text = (Convert.ToInt32(data[12]) + Convert.ToInt32(data[2])).ToString();
            tbServo3.Value = Convert.ToInt32(data[12]) + Convert.ToInt32(data[2]);
            lbServo4.Text = data[13];
            lbTrbServo4.Text = (Convert.ToInt32(data[13]) + Convert.ToInt32(data[3])).ToString();
            tbServo4.Value = Convert.ToInt32(data[13]) + Convert.ToInt32(data[3]);
        }
    }
}