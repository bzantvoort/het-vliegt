﻿namespace DroneMonitorTool {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnLog = new System.Windows.Forms.Button();
            this.lbConnected = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.cbBaudrate = new System.Windows.Forms.ComboBox();
            this.btnScanPorts = new System.Windows.Forms.Button();
            this.cbPorts = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbServo1 = new System.Windows.Forms.Label();
            this.lbServo2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbServo4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbServo3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbTrbServo4 = new System.Windows.Forms.Label();
            this.lbTrbServo3 = new System.Windows.Forms.Label();
            this.lbTrbServo2 = new System.Windows.Forms.Label();
            this.lbTrbServo1 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lbServoStab4 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lbServoStab2 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbServoStab3 = new System.Windows.Forms.Label();
            this.lbServoStab1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbServo4 = new System.Windows.Forms.TrackBar();
            this.label9 = new System.Windows.Forms.Label();
            this.tbServo3 = new System.Windows.Forms.TrackBar();
            this.label7 = new System.Windows.Forms.Label();
            this.tbServo2 = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.tbServo1 = new System.Windows.Forms.TrackBar();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbServo13Accel = new System.Windows.Forms.Label();
            this.lbServo24Accel = new System.Windows.Forms.Label();
            this.lbHoogtePos = new System.Windows.Forms.Label();
            this.lbServo13Pos = new System.Windows.Forms.Label();
            this.lbServo24Pos = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbServo24Pos = new System.Windows.Forms.TrackBar();
            this.tbServo13Pos = new System.Windows.Forms.TrackBar();
            this.tbHoogteAccel = new System.Windows.Forms.TrackBar();
            this.tbServo24Accel = new System.Windows.Forms.TrackBar();
            this.tbServo13Accel = new System.Windows.Forms.TrackBar();
            this.tbRotation = new System.Windows.Forms.TrackBar();
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.lbRotation = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo1)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo24Pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo13Pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbHoogteAccel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo24Accel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo13Accel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRotation)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::DroneMonitorTool.Properties.Resources.Drone;
            this.pictureBox1.Location = new System.Drawing.Point(6, 40);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(240, 191);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.btnLog);
            this.groupBox1.Controls.Add(this.lbConnected);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.btnConnect);
            this.groupBox1.Controls.Add(this.cbBaudrate);
            this.groupBox1.Controls.Add(this.btnScanPorts);
            this.groupBox1.Controls.Add(this.cbPorts);
            this.groupBox1.Location = new System.Drawing.Point(-3, -14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(591, 50);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Port";
            // 
            // btnLog
            // 
            this.btnLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLog.Location = new System.Drawing.Point(550, 19);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(35, 23);
            this.btnLog.TabIndex = 32;
            this.btnLog.Text = "Log";
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // lbConnected
            // 
            this.lbConnected.AutoSize = true;
            this.lbConnected.Location = new System.Drawing.Point(404, 24);
            this.lbConnected.Name = "lbConnected";
            this.lbConnected.Size = new System.Drawing.Size(16, 13);
            this.lbConnected.TabIndex = 30;
            this.lbConnected.Text = "...";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(339, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "Connected: ";
            // 
            // btnConnect
            // 
            this.btnConnect.Enabled = false;
            this.btnConnect.Location = new System.Drawing.Point(258, 19);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 3;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // cbBaudrate
            // 
            this.cbBaudrate.Enabled = false;
            this.cbBaudrate.FormattingEnabled = true;
            this.cbBaudrate.Items.AddRange(new object[] {
            "300",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.cbBaudrate.Location = new System.Drawing.Point(177, 21);
            this.cbBaudrate.Name = "cbBaudrate";
            this.cbBaudrate.Size = new System.Drawing.Size(75, 21);
            this.cbBaudrate.TabIndex = 2;
            this.cbBaudrate.Text = "Baudrate";
            // 
            // btnScanPorts
            // 
            this.btnScanPorts.Location = new System.Drawing.Point(15, 19);
            this.btnScanPorts.Name = "btnScanPorts";
            this.btnScanPorts.Size = new System.Drawing.Size(75, 23);
            this.btnScanPorts.TabIndex = 0;
            this.btnScanPorts.Text = "Scan";
            this.btnScanPorts.UseVisualStyleBackColor = true;
            this.btnScanPorts.Click += new System.EventHandler(this.btnScanPorts_Click);
            // 
            // cbPorts
            // 
            this.cbPorts.Enabled = false;
            this.cbPorts.FormattingEnabled = true;
            this.cbPorts.Location = new System.Drawing.Point(96, 21);
            this.cbPorts.Name = "cbPorts";
            this.cbPorts.Size = new System.Drawing.Size(75, 21);
            this.cbPorts.TabIndex = 1;
            this.cbPorts.Text = "Port";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Servo1: ";
            // 
            // lbServo1
            // 
            this.lbServo1.AutoSize = true;
            this.lbServo1.Location = new System.Drawing.Point(56, 18);
            this.lbServo1.Name = "lbServo1";
            this.lbServo1.Size = new System.Drawing.Size(16, 13);
            this.lbServo1.TabIndex = 3;
            this.lbServo1.Text = "...";
            // 
            // lbServo2
            // 
            this.lbServo2.AutoSize = true;
            this.lbServo2.Location = new System.Drawing.Point(56, 215);
            this.lbServo2.Name = "lbServo2";
            this.lbServo2.Size = new System.Drawing.Size(16, 13);
            this.lbServo2.TabIndex = 5;
            this.lbServo2.Text = "...";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Servo2: ";
            // 
            // lbServo4
            // 
            this.lbServo4.AutoSize = true;
            this.lbServo4.Location = new System.Drawing.Point(228, 18);
            this.lbServo4.Name = "lbServo4";
            this.lbServo4.Size = new System.Drawing.Size(16, 13);
            this.lbServo4.TabIndex = 9;
            this.lbServo4.Text = "...";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(183, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Servo4: ";
            // 
            // lbServo3
            // 
            this.lbServo3.AutoSize = true;
            this.lbServo3.Location = new System.Drawing.Point(228, 215);
            this.lbServo3.Name = "lbServo3";
            this.lbServo3.Size = new System.Drawing.Size(16, 13);
            this.lbServo3.TabIndex = 7;
            this.lbServo3.Text = "...";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(183, 215);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Servo3: ";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.lbTrbServo4);
            this.groupBox2.Controls.Add(this.lbTrbServo3);
            this.groupBox2.Controls.Add(this.lbTrbServo2);
            this.groupBox2.Controls.Add(this.lbTrbServo1);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.lbServoStab4);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.lbServoStab2);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.lbServoStab3);
            this.groupBox2.Controls.Add(this.lbServoStab1);
            this.groupBox2.Controls.Add(this.lbServo3);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.tbServo4);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.tbServo3);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.tbServo2);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.lbServo4);
            this.groupBox2.Controls.Add(this.tbServo1);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.lbServo1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.lbServo2);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Location = new System.Drawing.Point(12, 42);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(253, 507);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Servos";
            // 
            // lbTrbServo4
            // 
            this.lbTrbServo4.AutoSize = true;
            this.lbTrbServo4.Location = new System.Drawing.Point(214, 378);
            this.lbTrbServo4.Name = "lbTrbServo4";
            this.lbTrbServo4.Size = new System.Drawing.Size(16, 13);
            this.lbTrbServo4.TabIndex = 38;
            this.lbTrbServo4.Text = "...";
            // 
            // lbTrbServo3
            // 
            this.lbTrbServo3.AutoSize = true;
            this.lbTrbServo3.Location = new System.Drawing.Point(159, 378);
            this.lbTrbServo3.Name = "lbTrbServo3";
            this.lbTrbServo3.Size = new System.Drawing.Size(16, 13);
            this.lbTrbServo3.TabIndex = 37;
            this.lbTrbServo3.Text = "...";
            // 
            // lbTrbServo2
            // 
            this.lbTrbServo2.AutoSize = true;
            this.lbTrbServo2.Location = new System.Drawing.Point(99, 378);
            this.lbTrbServo2.Name = "lbTrbServo2";
            this.lbTrbServo2.Size = new System.Drawing.Size(16, 13);
            this.lbTrbServo2.TabIndex = 36;
            this.lbTrbServo2.Text = "...";
            // 
            // lbTrbServo1
            // 
            this.lbTrbServo1.AutoSize = true;
            this.lbTrbServo1.Location = new System.Drawing.Point(41, 378);
            this.lbTrbServo1.Name = "lbTrbServo1";
            this.lbTrbServo1.Size = new System.Drawing.Size(16, 13);
            this.lbTrbServo1.TabIndex = 35;
            this.lbTrbServo1.Text = "...";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(183, 31);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 13);
            this.label19.TabIndex = 27;
            this.label19.Text = "Stab:";
            // 
            // lbServoStab4
            // 
            this.lbServoStab4.AutoSize = true;
            this.lbServoStab4.Location = new System.Drawing.Point(228, 31);
            this.lbServoStab4.Name = "lbServoStab4";
            this.lbServoStab4.Size = new System.Drawing.Size(16, 13);
            this.lbServoStab4.TabIndex = 28;
            this.lbServoStab4.Text = "...";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(9, 228);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(32, 13);
            this.label21.TabIndex = 25;
            this.label21.Text = "Stab:";
            // 
            // lbServoStab2
            // 
            this.lbServoStab2.AutoSize = true;
            this.lbServoStab2.Location = new System.Drawing.Point(56, 228);
            this.lbServoStab2.Name = "lbServoStab2";
            this.lbServoStab2.Size = new System.Drawing.Size(16, 13);
            this.lbServoStab2.TabIndex = 26;
            this.lbServoStab2.Text = "...";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(183, 228);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(32, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "Stab:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 31);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "Stab:";
            // 
            // lbServoStab3
            // 
            this.lbServoStab3.AutoSize = true;
            this.lbServoStab3.Location = new System.Drawing.Point(228, 228);
            this.lbServoStab3.Name = "lbServoStab3";
            this.lbServoStab3.Size = new System.Drawing.Size(16, 13);
            this.lbServoStab3.TabIndex = 24;
            this.lbServoStab3.Text = "...";
            // 
            // lbServoStab1
            // 
            this.lbServoStab1.AutoSize = true;
            this.lbServoStab1.Location = new System.Drawing.Point(56, 31);
            this.lbServoStab1.Name = "lbServoStab1";
            this.lbServoStab1.Size = new System.Drawing.Size(16, 13);
            this.lbServoStab1.TabIndex = 22;
            this.lbServoStab1.Text = "...";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(183, 253);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Servo4: ";
            // 
            // tbServo4
            // 
            this.tbServo4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbServo4.Enabled = false;
            this.tbServo4.Location = new System.Drawing.Point(186, 269);
            this.tbServo4.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.tbServo4.Maximum = 180;
            this.tbServo4.Minimum = 1;
            this.tbServo4.Name = "tbServo4";
            this.tbServo4.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbServo4.Size = new System.Drawing.Size(45, 232);
            this.tbServo4.TabIndex = 20;
            this.tbServo4.Value = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(125, 253);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Servo3: ";
            // 
            // tbServo3
            // 
            this.tbServo3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbServo3.Enabled = false;
            this.tbServo3.Location = new System.Drawing.Point(128, 269);
            this.tbServo3.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.tbServo3.Maximum = 180;
            this.tbServo3.Minimum = 1;
            this.tbServo3.Name = "tbServo3";
            this.tbServo3.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbServo3.Size = new System.Drawing.Size(45, 232);
            this.tbServo3.TabIndex = 18;
            this.tbServo3.Value = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(67, 253);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Servo2: ";
            // 
            // tbServo2
            // 
            this.tbServo2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbServo2.Enabled = false;
            this.tbServo2.Location = new System.Drawing.Point(70, 269);
            this.tbServo2.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.tbServo2.Maximum = 180;
            this.tbServo2.Minimum = 1;
            this.tbServo2.Name = "tbServo2";
            this.tbServo2.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbServo2.Size = new System.Drawing.Size(45, 232);
            this.tbServo2.TabIndex = 16;
            this.tbServo2.Value = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 253);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Servo1: ";
            // 
            // tbServo1
            // 
            this.tbServo1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbServo1.Enabled = false;
            this.tbServo1.Location = new System.Drawing.Point(12, 269);
            this.tbServo1.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.tbServo1.Maximum = 180;
            this.tbServo1.Minimum = 1;
            this.tbServo1.Name = "tbServo1";
            this.tbServo1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbServo1.Size = new System.Drawing.Size(45, 232);
            this.tbServo1.TabIndex = 11;
            this.tbServo1.Value = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.tbRotation);
            this.groupBox3.Controls.Add(this.lbRotation);
            this.groupBox3.Controls.Add(this.lbServo13Accel);
            this.groupBox3.Controls.Add(this.lbServo24Accel);
            this.groupBox3.Controls.Add(this.lbHoogtePos);
            this.groupBox3.Controls.Add(this.lbServo13Pos);
            this.groupBox3.Controls.Add(this.lbServo24Pos);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.tbServo24Pos);
            this.groupBox3.Controls.Add(this.tbServo13Pos);
            this.groupBox3.Controls.Add(this.tbHoogteAccel);
            this.groupBox3.Controls.Add(this.tbServo24Accel);
            this.groupBox3.Controls.Add(this.tbServo13Accel);
            this.groupBox3.Location = new System.Drawing.Point(271, 42);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(301, 430);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Gyro";
            // 
            // lbServo13Accel
            // 
            this.lbServo13Accel.AutoSize = true;
            this.lbServo13Accel.Location = new System.Drawing.Point(143, 302);
            this.lbServo13Accel.Name = "lbServo13Accel";
            this.lbServo13Accel.Size = new System.Drawing.Size(16, 13);
            this.lbServo13Accel.TabIndex = 33;
            this.lbServo13Accel.Text = "...";
            // 
            // lbServo24Accel
            // 
            this.lbServo24Accel.AutoSize = true;
            this.lbServo24Accel.Location = new System.Drawing.Point(143, 237);
            this.lbServo24Accel.Name = "lbServo24Accel";
            this.lbServo24Accel.Size = new System.Drawing.Size(16, 13);
            this.lbServo24Accel.TabIndex = 32;
            this.lbServo24Accel.Text = "...";
            // 
            // lbHoogtePos
            // 
            this.lbHoogtePos.AutoSize = true;
            this.lbHoogtePos.Location = new System.Drawing.Point(143, 172);
            this.lbHoogtePos.Name = "lbHoogtePos";
            this.lbHoogtePos.Size = new System.Drawing.Size(16, 13);
            this.lbHoogtePos.TabIndex = 31;
            this.lbHoogtePos.Text = "...";
            // 
            // lbServo13Pos
            // 
            this.lbServo13Pos.AutoSize = true;
            this.lbServo13Pos.Location = new System.Drawing.Point(143, 107);
            this.lbServo13Pos.Name = "lbServo13Pos";
            this.lbServo13Pos.Size = new System.Drawing.Size(16, 13);
            this.lbServo13Pos.TabIndex = 30;
            this.lbServo13Pos.Text = "...";
            // 
            // lbServo24Pos
            // 
            this.lbServo24Pos.AutoSize = true;
            this.lbServo24Pos.Location = new System.Drawing.Point(143, 42);
            this.lbServo24Pos.Name = "lbServo24Pos";
            this.lbServo24Pos.Size = new System.Drawing.Size(16, 13);
            this.lbServo24Pos.TabIndex = 29;
            this.lbServo24Pos.Text = "...";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 95);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(86, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "Servo 1-3 positie";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(86, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "Servo 2-4 positie";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 160);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Hoogte acceleratie";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 225);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Servo 2-4 acceleratie";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 290);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Servo 1-3 acceleratie";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 355);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Rotatie";
            // 
            // tbServo24Pos
            // 
            this.tbServo24Pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbServo24Pos.Enabled = false;
            this.tbServo24Pos.Location = new System.Drawing.Point(6, 53);
            this.tbServo24Pos.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tbServo24Pos.Maximum = 32767;
            this.tbServo24Pos.Minimum = -32767;
            this.tbServo24Pos.Name = "tbServo24Pos";
            this.tbServo24Pos.Size = new System.Drawing.Size(289, 45);
            this.tbServo24Pos.TabIndex = 20;
            // 
            // tbServo13Pos
            // 
            this.tbServo13Pos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbServo13Pos.Enabled = false;
            this.tbServo13Pos.Location = new System.Drawing.Point(6, 118);
            this.tbServo13Pos.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tbServo13Pos.Maximum = 32767;
            this.tbServo13Pos.Minimum = -32767;
            this.tbServo13Pos.Name = "tbServo13Pos";
            this.tbServo13Pos.Size = new System.Drawing.Size(289, 45);
            this.tbServo13Pos.TabIndex = 19;
            // 
            // tbHoogteAccel
            // 
            this.tbHoogteAccel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbHoogteAccel.Enabled = false;
            this.tbHoogteAccel.Location = new System.Drawing.Point(6, 183);
            this.tbHoogteAccel.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tbHoogteAccel.Maximum = 32767;
            this.tbHoogteAccel.Minimum = -32767;
            this.tbHoogteAccel.Name = "tbHoogteAccel";
            this.tbHoogteAccel.Size = new System.Drawing.Size(289, 45);
            this.tbHoogteAccel.TabIndex = 18;
            // 
            // tbServo24Accel
            // 
            this.tbServo24Accel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbServo24Accel.Enabled = false;
            this.tbServo24Accel.Location = new System.Drawing.Point(6, 248);
            this.tbServo24Accel.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tbServo24Accel.Maximum = 32767;
            this.tbServo24Accel.Minimum = -32767;
            this.tbServo24Accel.Name = "tbServo24Accel";
            this.tbServo24Accel.Size = new System.Drawing.Size(289, 45);
            this.tbServo24Accel.TabIndex = 17;
            // 
            // tbServo13Accel
            // 
            this.tbServo13Accel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbServo13Accel.Enabled = false;
            this.tbServo13Accel.Location = new System.Drawing.Point(6, 313);
            this.tbServo13Accel.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tbServo13Accel.Maximum = 32767;
            this.tbServo13Accel.Minimum = -32767;
            this.tbServo13Accel.Name = "tbServo13Accel";
            this.tbServo13Accel.Size = new System.Drawing.Size(289, 45);
            this.tbServo13Accel.TabIndex = 16;
            // 
            // tbRotation
            // 
            this.tbRotation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRotation.Enabled = false;
            this.tbRotation.Location = new System.Drawing.Point(6, 378);
            this.tbRotation.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.tbRotation.Maximum = 32767;
            this.tbRotation.Minimum = -32767;
            this.tbRotation.Name = "tbRotation";
            this.tbRotation.Size = new System.Drawing.Size(289, 45);
            this.tbRotation.TabIndex = 15;
            // 
            // timer
            // 
            this.timer.Interval = 50;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // lbRotation
            // 
            this.lbRotation.AutoSize = true;
            this.lbRotation.Location = new System.Drawing.Point(143, 367);
            this.lbRotation.Name = "lbRotation";
            this.lbRotation.Size = new System.Drawing.Size(16, 13);
            this.lbRotation.TabIndex = 34;
            this.lbRotation.Text = "...";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(584, 561);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MinimumSize = new System.Drawing.Size(520, 600);
            this.Name = "Form1";
            this.Text = "Drone Monitor Tool";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo24Pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo13Pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbHoogteAccel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo24Accel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServo13Accel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRotation)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbServo1;
        private System.Windows.Forms.Label lbServo2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbServo4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbServo3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TrackBar tbServo4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TrackBar tbServo3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TrackBar tbServo2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar tbServo1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TrackBar tbServo24Pos;
        private System.Windows.Forms.TrackBar tbServo13Pos;
        private System.Windows.Forms.TrackBar tbHoogteAccel;
        private System.Windows.Forms.TrackBar tbServo24Accel;
        private System.Windows.Forms.TrackBar tbServo13Accel;
        private System.Windows.Forms.TrackBar tbRotation;
        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.Button btnScanPorts;
        private System.Windows.Forms.ComboBox cbPorts;
        private System.Windows.Forms.ComboBox cbBaudrate;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lbServoStab4;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lbServoStab2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbServoStab3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbServoStab1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbConnected;
        private System.Windows.Forms.Label lbServo24Pos;
        private System.Windows.Forms.Label lbServo13Accel;
        private System.Windows.Forms.Label lbServo24Accel;
        private System.Windows.Forms.Label lbHoogtePos;
        private System.Windows.Forms.Label lbServo13Pos;
        private System.Windows.Forms.Label lbTrbServo4;
        private System.Windows.Forms.Label lbTrbServo3;
        private System.Windows.Forms.Label lbTrbServo2;
        private System.Windows.Forms.Label lbTrbServo1;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button btnLog;
        private System.Windows.Forms.Label lbRotation;
    }
}

