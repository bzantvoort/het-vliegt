#include <OneSheeld.h>

void setup() {
  // put your setup code here, to run once:
  OneSheeld.begin();
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("button pressed: ");
  if (GamePad.isRedPressed()) {
    Serial.println("RED");
  }
  if (GamePad.isGreenPressed()) {
    Serial.println("GREEN");
  }
  if (GamePad.isOrangePressed()) {
    Serial.println("ORANGE");
  }
  if (GamePad.isBluePressed()) {
    Serial.println("BLUE");
  }
}
