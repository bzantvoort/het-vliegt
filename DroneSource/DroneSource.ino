#include <MPU6050.h>
#include <OneSheeld.h>
#include <Servo.h>
#include <Wire.h>
#include <I2Cdev.h>
#include <stdio.h>

MPU6050 accelgyro;
int16_t ax, ay, az;
int16_t gx, gy, gz;

#define OUTPUT_READABLE_ACCELGYRO
//#define LOGGING


Servo Servo1;
Servo Servo2;
Servo Servo3;
Servo Servo4;

/*?????*/
byte hoverMode = 115;
/*?????*/

short gX[10];
short gY[10];
short gFlip[10];

short aX[10];
short aY[10];
short aSpin[10];

byte arrayCount;

short Servo1Stab;
short Servo2Stab;
short Servo3Stab;
short Servo4Stab;

void setup() {
  // put your setup code here, to run once:
  Wire.begin();
  OneSheeld.begin();
  Serial.begin(115200);

  Servo1.attach(5);
  Servo2.attach(9);
  Servo3.attach(10);
  Servo4.attach(11);

  delay(2);
  Servo1.write(50);
  Servo2.write(50);
  Servo3.write(50);
  Servo4.write(50);

  Serial.println("Initializing I2C devices...");
  accelgyro.initialize();

  // verify connection
  Serial.println("Testing device connections...");
  Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");
}

void loop() {

  // if (OneSheeld.isAppConnected()) {
  if (GamePad.isRedPressed()) {
    #ifdef LOGGING
    Serial.println("Red pressed");
    #endif
    ServoControl(20);
  }

  if (GamePad.isOrangePressed()) {
    #ifdef LOGGING
    Serial.println("Orange pressed");
    #endif
    ServoControl(90);
  }

  if (GamePad.isGreenPressed()) {
    #ifdef LOGGING
    Serial.println("Green pressed");
    #endif
    ServoControl(100);
  }

  if (GamePad.isBluePressed()) {
    #ifdef LOGGING
    Serial.println("Blue pressed");
    #endif
    ServoControl(110);
  }
  // }
  //  else  {
  //    Servo1.write(20);
  //    Servo2.write(20);
  //    Servo3.write(20);
  //    Servo4.write(20);
  //  }



  accelgyro.getAcceleration(&ax, &ay, &az);
  accelgyro.getRotation(&gx, &gy, &gz);

#ifdef OUTPUT_READABLE_ACCELGYRO

  if (arrayCount < 10) {
    gX[arrayCount] = ax;
    gY[arrayCount] = ay;
    gFlip[arrayCount] = az;

    aX[arrayCount] = gx;
    aY[arrayCount] = gy;
    aSpin[arrayCount] = gz;

    arrayCount = arrayCount + 1;
  }
  else if (arrayCount == 10) {

    Stabilize();
    arrayCount = 0;
  
    Serial.println("#" + 
                 (String)Servo1Stab + "|" + (String)Servo2Stab + "|" + (String)Servo3Stab + "|" + (String)Servo4Stab + "|" + 
                 (String)ax + "|" +  (String)ay + "|" + (String)az + "|" + (String)gx + "|" + (String)gy + "|" + (String)gz + 
                 "|" + Servo1.read() + "|" + Servo2.read() + "|" + Servo3.read() + "|" + Servo4.read() + "%" + "\r");
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
#endif

#ifdef OUTPUT_BINARY_ACCELGYRO
  Serial.write((uint8_t)(ax >> 8)); Serial.write((uint8_t)(ax & 0xFF));
  Serial.write((uint8_t)(ay >> 8)); Serial.write((uint8_t)(ay & 0xFF));
  Serial.write((uint8_t)(az >> 8)); Serial.write((uint8_t)(az & 0xFF));
  Serial.write((uint8_t)(gx >> 8)); Serial.write((uint8_t)(gx & 0xFF));
  Serial.write((uint8_t)(gy >> 8)); Serial.write((uint8_t)(gy & 0xFF));
  Serial.write((uint8_t)(gz >> 8)); Serial.write((uint8_t)(gz & 0xFF));
#endif



}


void Stabilize() {
  #ifdef LOGGING
  Serial.println("Stabilize");
  Serial.print("a/g:\t");
  Serial.print(Average(gX)); Serial.print("\t");
  Serial.print(Average(gY)); Serial.print("\t");
  Serial.print(Average(gFlip)); Serial.print("\t | \t");
  Serial.print(Average(aX)); Serial.print("\t");
  Serial.print(Average(aY)); Serial.print("\t");
  Serial.println(Average(aSpin));
  Serial.println("");
  #endif

  long AverageGX = Average(gX);
  if (AverageGX < 500 && AverageGX > -500)
  {
    Servo2Stab = 0;
    Servo4Stab = 0;
  }
  else if (AverageGX > 500 && AverageGX < 1000)
  {
    Servo2Stab = -2;
    Servo4Stab = 2;
  }
  else if (AverageGX < -500 && AverageGX > - 1000)
  {
    Servo2Stab = 2;
    Servo4Stab = -2;
  }
  else if (AverageGX > 1000 && AverageGX < 1500)
  {
    Servo2Stab = -4;
    Servo4Stab = 4;
  }
  else if (AverageGX < -1000 && AverageGX > - 1500)
  {
    Servo2Stab = 4;
    Servo4Stab = -4;
  }
  else if (AverageGX > 1500 && AverageGX < 2000)
  {
    Servo2Stab = -6;
    Servo4Stab = 6;
  }
  else if (AverageGX < -1500 && AverageGX > - 2000)
  {
    Servo2Stab = 6;
    Servo4Stab = -6;
  }
  else if (AverageGX > 2000 && AverageGX < 2500)
  {
    Servo2Stab = -8;
    Servo4Stab = 8;
  }
  else if (AverageGX < -2000 && AverageGX > -2500)
  {
    Servo2Stab = 8;
    Servo4Stab = -8;
  }
  else if (AverageGX > 2500 || AverageGX < -2500)
  {
    Servo2Stab = -10;
    Servo4Stab = 10;
  }

  long AverageGY = Average(gY);
  if (AverageGY < 500 && AverageGY > -500)
  {
    Servo1Stab = 0;
    Servo3Stab = 0;
  }
  else if (AverageGY > 500 && AverageGY < 1000)
  {
    Servo1Stab = -2;
    Servo3Stab = 2;
  }
  else if (AverageGY < -500 && AverageGY > - 1000)
  {
    Servo1Stab = 2;
    Servo3Stab = -2;
  }
  else if (AverageGY > 1000 && AverageGY < 1500)
  {
    Servo1Stab = -4;
    Servo3Stab = 4;
  }
  else if (AverageGY < -1000 && AverageGY > - 1500)
  {
    Servo1Stab = 4;
    Servo3Stab = -4;
  }
  else if (AverageGY > 1500 && AverageGY < 2000)
  {
    Servo1Stab = -6;
    Servo3Stab = 6;
  }
  else if (AverageGY < -1500 && AverageGY  > - 2000)
  {
    Servo1Stab = 6;
    Servo3Stab = -6;
  }
  else if (AverageGY > 2000 && AverageGY < 2500)
  {
    Servo1Stab = -8;
    Servo3Stab = 8;
  }
  else if (AverageGY < -2000 && AverageGY > -2500)
  {
    Servo1Stab = 8;
    Servo3Stab = -8;
  }
  else if (AverageGY > 2500 || AverageGY < -2500)
  {
    Servo1Stab = -10;
    Servo3Stab = 10;
  }
#ifdef LOGGING
  Serial.print("Servo1Stab: "); Serial.println(Servo1Stab);
  Serial.print("Servo2Stab: "); Serial.println(Servo2Stab);
  Serial.print("Servo3Stab: "); Serial.println(Servo3Stab);
  Serial.print("Servo4Stab: "); Serial.println(Servo4Stab);
  #endif

}

long Average(short array[10]) {
  long sum = 0;
  for (int i = 0; i < 10; ++i) {
    sum += array[i];
    //Serial.print("sum: ");Serial.println(sum);
  }
  return sum / 10;
}

void ServoControl(int servoSpeed) {

  delay(250);
  Servo1.write(servoSpeed + Servo1Stab);
  Servo2.write(servoSpeed + Servo2Stab);
  Servo3.write(servoSpeed + Servo3Stab);
  Servo4.write(servoSpeed + Servo4Stab);
#ifdef LOGGING
  Serial.println(Servo1.read());
  Serial.println(Servo2.read());
  Serial.println(Servo3.read());
  Serial.println(Servo4.read());
  #endif
}
